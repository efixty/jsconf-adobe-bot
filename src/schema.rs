diesel::table! {
    message(id) {
        id -> BigInt,
        message_id -> BigInt,
        user_id -> BigInt,
        text -> Varchar,
        incoming -> Bool,
    }
}

diesel::table! {
    quiz_message(message_id) {
        message_id -> BigInt,
        user_id -> BigInt,
        quiz_question_id -> BigInt,
        answer -> BigInt,
        quiz_id -> BigInt,
    }
}

diesel::table! {
    quiz_question(id) {
        id -> BigInt,
        question_text -> Varchar,
        correct_answer_option_id -> BigInt,
        topic_id -> BigInt,
    }
}

diesel::table! {
    answer_option(id) {
        id -> BigInt,
        option_text -> Varchar,
        question_id -> BigInt,
    }
}

diesel::table! {
    users(id) {
        id -> BigInt,
        chat_id -> BigInt,
        first_name -> Varchar,
        last_name -> Nullable<Varchar>,
        username -> Nullable<Varchar>,
        last_quiz_id -> Nullable<BigInt>,
    }
}

diesel::table! {
    quiz(id) {
        id -> BigInt,
        topic_id -> Bigint,
        user_id -> BigInt,
        aborted -> Bool,
        finished -> Bool,
    }
}

diesel::table! {
    topic(id) {
        id -> BigInt,
        name -> Varchar,
    }
}

joinable!(answer_option -> quiz_question (question_id));
allow_tables_to_appear_in_same_query!(answer_option, quiz_question,);
