use crate::model::{internal, telegram, telegram::Result};
use log::{error, info};

pub async fn send_plain_text(chat_id: i64, text: String) -> Result<telegram::Message> {
    let simple_text_message: telegram::send::Message = telegram::send::Message {
        chat_id,
        text,
        parse_mode: None,
        reply_to_message_id: None,
        reply_markup: None,
    };
    return send_message(simple_text_message).await;
}

pub async fn send_custom_message(message: telegram::send::Message) -> Result<telegram::Message> {
    return send_message(message).await;
}

pub async fn send_quiz_question(
    chat_id: i64,
    question: internal::QuizQuestion,
) -> Result<telegram::Message> {
    let quiz_question_message = telegram::send::Message {
        chat_id,
        text: question.question,
        parse_mode: Some(telegram::ParseMode::Html.value()),
        reply_to_message_id: None,
        reply_markup: Some(telegram::send::InlineKeyboardMarkup {
            inline_keyboard: vec![question.options_buttons],
        }),
    };
    return send_message(quiz_question_message).await;
}

pub async fn send_score_message(
    chat_id: i64,
    completion: internal::QuizCompletion,
) -> Result<telegram::Message> {
    let quiz_completion_message = telegram::send::Message {
        chat_id,
        text: format!(
            "Your score is {}\n{}\n",
            completion.score, completion.completion_salut
        ),
        parse_mode: None,
        reply_to_message_id: None,
        reply_markup: None, // add "mistakes" button
    };

    return send_message(quiz_completion_message).await;
}

pub async fn send_quiz_topics(
    chat_id: i64,
    topics: Vec<internal::Topic>,
) -> Result<telegram::Message> {
    let topic_buttons = topics
        .into_iter()
        .map(telegram::send::InlineKeyboardButton::from)
        .collect::<Vec<telegram::send::InlineKeyboardButton>>();
    let topics_reply_markup = telegram::send::InlineKeyboardMarkup {
        inline_keyboard: vec![topic_buttons[0..2].to_vec(), topic_buttons[2..].to_vec()],
    };
    let topics = telegram::send::Message {
        chat_id,
        text: String::from("choose a topic 📚"),
        parse_mode: None,
        reply_to_message_id: None,
        reply_markup: Some(topics_reply_markup),
    };
    return send_message(topics).await;
}

pub async fn edit_message(edit_payload: telegram::send::EditMessage) {
    let (telegram_client, telegram_api_base_url) = telegram_request_data();
    let response = telegram_client
        .post(format!("{}/editMessageText", telegram_api_base_url))
        .json(&edit_payload)
        .send()
        .await;
    if let Ok(response) = response {
        info!("send edit status is {}", response.status());
    } else {
        error!(
            "message editing failure, details: {:?}",
            response.unwrap_err()
        )
    }
}

async fn send_message(message: telegram::send::Message) -> Result<telegram::Message> {
    let (telegram_client, telegram_api_base_url) = telegram_request_data();
    let response = telegram_client
        .post(format!("{}/sendMessage", telegram_api_base_url))
        .json(&message)
        .send()
        .await;

    if let Ok(response) = response {
        info!("send message status is {}", &response.status());
        return response.json::<telegram::Message>().await;
    } else {
        error!(
            "message sending failure, details: {:?}",
            response.as_ref().unwrap_err()
        );
        return Err(response.unwrap_err());
    }
}

fn telegram_request_data() -> (reqwest::Client, String) {
    return (
        reqwest::Client::new(),
        std::env::var("TELEGRAM_BOT_API").unwrap(),
    );
}
