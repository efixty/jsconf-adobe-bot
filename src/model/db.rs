use crate::schema::*;
use diesel::{Insertable, Queryable};

#[derive(Debug, Queryable, QueryableByName)]
pub struct ScoreContainer {
    #[diesel(sql_type = diesel::sql_types::Integer)]
    pub score: i32,
}

#[derive(Debug, Queryable, QueryableByName)]
pub struct IdContainer {
    #[diesel(sql_type = diesel::sql_types::BigInt)]
    pub id: i64   
}

#[derive(Debug, Queryable, Insertable)]
#[diesel(table_name = users)]
pub struct User {
    pub id: i64,
    pub chat_id: i64,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: Option<String>,
    pub last_quiz_id: Option<i64>
}

impl From<&super::telegram::Message> for User {
    fn from(message: &super::telegram::Message) -> User {
        return User {
            id: message.from.id,
            chat_id: message.chat.id,
            first_name: message.from.first_name.clone(),
            last_name: message.from.last_name.clone(),
            username: message.from.username.clone(),
            last_quiz_id: None,
        };
    }
}

#[derive(Debug, Queryable)]
#[diesel(table_name = message)]
pub struct Message {
    pub id: i64,
    pub message_id: i64,
    pub user_id: i64,
    pub text: String,
    pub incoming: bool,
}

#[derive(Debug, Insertable)]
#[diesel(table_name = message)]
pub struct NewMessage {
    pub message_id: i64,
    pub user_id: i64,
    pub text: String,
    pub incoming: bool,
}

impl From<(&super::telegram::Message, bool)> for NewMessage {
    fn from((message, incoming): (&super::telegram::Message, bool)) -> NewMessage {
        return NewMessage {
            message_id: message.message_id,
            user_id: message.from.id,
            text: message.text.clone(),
            incoming,
        };
    }
}

#[derive(Debug, Queryable, QueryableByName, Insertable)]
#[diesel(table_name = quiz_message)]
pub struct QuizMessage {
    pub message_id: i64,
    pub user_id: i64,
    pub quiz_question_id: i64,
    pub answer: i64,
    pub quiz_id: i64,
}

#[derive(Debug, Queryable, QueryableByName)]
#[diesel(table_name = quiz_question)]
pub struct QuizQuestion {
    pub id: i64,
    pub question_text: String,
    pub correct_answer_option_id: i64,
    pub topic_id: i64,
}

#[derive(Debug, Queryable, Insertable)]
#[diesel(table_name = answer_option)]
pub struct AnswerOption {
    pub id: i64,
    pub option_text: String,
    pub question_id: i64,
}

#[derive(Debug, Insertable)]
#[diesel(table_name = quiz)]
pub struct NewQuiz {
    pub topic_id: i64,
    pub user_id: i64,
}

#[derive(Debug, Queryable, QueryableByName)]
#[diesel(table_name = quiz)]
pub struct Quiz {
    pub id: i64,
    pub topic_id: i64,
    pub user_id: i64,
    pub aborted: bool,
    pub finished: bool,
}

#[derive(Debug, Queryable)]
#[diesel(table_name = topic)]
pub struct Topic {
    pub id: i64,
    pub name: String,
}
