use super::{*, telegram::send::InlineKeyboardButton};
use std::collections;

const PASSED_PHRASE: &str = "Congrats!🎉\nPsss... we can always discuss it together!";
const FAILED_PHRASE: &str = "Thanks for taking the challenge! That was tough\nBtw, you can always retake that quiz🙃";

#[derive(Debug)]
pub struct Question {
    pub id: i64,
    pub question_text: String,
    pub correct_answer_option_id: i64,
    pub answer_options: Vec<AnswerOption>,
}

pub struct QuizQuestion {
    pub question: String,
    pub options_buttons: Vec<InlineKeyboardButton>
}

pub struct Topic {
    pub id: i64,
    pub name: String,
}

impl From<db::Topic> for Topic {
    fn from(db_topic: db::Topic) -> Self {
        return Self {
            id: db_topic.id,
            name: db_topic.name,
        };
    }
}

#[derive(Debug)]
pub struct AnswerCallbackPayload {
    pub topic_id: i64,
    pub question_id: i64,
    pub answer_id: i64,
    pub quiz_id: i64,
    pub answer_option_index: i8,
}

impl From<String> for AnswerCallbackPayload {
    fn from(callback_payload: String) -> Self {
        let answer = callback_payload.split("|").collect::<Vec<&str>>();
        let (question_id, selected_option_id, topic_id, quiz_id, answer_option_index) = (
            answer[0].parse::<i64>().unwrap(),
            answer[1].parse::<i64>().unwrap(),
            answer[2].parse::<i64>().unwrap(),
            answer[3].parse::<i64>().unwrap(),
            answer[4].parse::<i8>().unwrap(),
        );
        return Self {
            question_id,
            topic_id,
            answer_id: selected_option_id,
            quiz_id,
            answer_option_index,
        };
    }
}

#[derive(Debug)]
pub struct AnswerOption {
    pub id: i64,
    pub option_text: String,
}

pub struct QuizCompletion {
    pub score: i8,
    pub completion_salut: String,
}

impl From<i8> for QuizCompletion {
    fn from(score: i8) -> Self {
        let completion_salut = match score {
            1..=7 => String::from(FAILED_PHRASE),
            8..=10 => String::from(PASSED_PHRASE),
            _ => format!("impossible score {}", score),
        };
        return QuizCompletion {
            score,
            completion_salut,
        };
    }
}