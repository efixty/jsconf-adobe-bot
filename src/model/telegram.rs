use super::internal;
use serde::{Deserialize, Serialize};

pub type Result<T> = std::result::Result<T, reqwest::Error>;

#[derive(Debug, Serialize, Deserialize)]
pub enum ParseMode {
    Markdown,
    MarkdownV2,
    Html,
}

impl ParseMode {
    pub fn value(&self) -> String {
        return match self {
            ParseMode::Markdown => String::from("Markdown"),
            ParseMode::MarkdownV2 => String::from("MarkdownV2"),
            ParseMode::Html => String::from("HTML"),
        };
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub id: i64,
    pub is_bot: bool,
    pub first_name: String,
    pub last_name: Option<String>,
    pub username: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Update {
    pub update_id: i64,
    pub message: Option<Message>,
    pub callback_query: Option<CallbackQuery>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CallbackQuery {
    pub id: String,
    pub from: User,
    pub message: Message,
    pub data: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Message {
    pub message_id: i64,
    pub from: User,
    pub date: u64, // unix time,
    pub chat: Chat,
    pub text: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Chat {
    pub id: i64,
    #[serde(rename = "type")]
    pub chat_type: String, // enum
    pub first_name: String,
    pub last_name: Option<String>,
}

pub mod send {

    use super::*;

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Message {
        pub chat_id: i64,
        pub text: String,

        #[serde(skip_serializing_if = "Option::is_none")]
        pub parse_mode: Option<String>,

        #[serde(skip_serializing_if = "Option::is_none")]
        pub reply_to_message_id: Option<i64>,

        #[serde(skip_serializing_if = "Option::is_none")]
        pub reply_markup: Option<InlineKeyboardMarkup>,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct InlineKeyboardMarkup {
        pub inline_keyboard: Vec<Vec<InlineKeyboardButton>>,
    }

    impl<Displayable> From<std::collections::BTreeMap<Displayable, String>> for InlineKeyboardMarkup
    where
        Displayable: std::fmt::Display,
    {
        fn from(
            button_data: std::collections::BTreeMap<Displayable, String>,
        ) -> InlineKeyboardMarkup {
            let buttons = button_data
                .into_iter()
                .map(|(option_text, callback_data)| InlineKeyboardButton {
                    text: option_text.to_string(),
                    callback_data,
                })
                .collect::<Vec<InlineKeyboardButton>>();
            let button_rows = vec![buttons[0..2].to_vec(), buttons[2..4].to_vec()];
            println!("{:?}", button_rows);
            return InlineKeyboardMarkup {
                inline_keyboard: button_rows,
            };
        }
    }

    impl InlineKeyboardMarkup {
        pub fn quiz_abortion_reply_markup() -> Self {
            let abort_quiz = InlineKeyboardButton {
                text: String::from("Abort"),
                callback_data: String::from("a"),
            };
            return Self {
                inline_keyboard: vec![vec![abort_quiz]],
            };
        }
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct InlineKeyboardButton {
        pub text: String,
        pub callback_data: String,
    }

    impl From<internal::Topic> for InlineKeyboardButton {
        fn from(topic: internal::Topic) -> Self {
            return Self {
                text: topic.name,
                callback_data: format!("t{}", topic.id),
            };
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct EditMessage {
        pub chat_id: i64,
        pub message_id: i64,
        pub text: String,

        #[serde(skip_serializing_if = "Option::is_none")]
        pub parse_mode: Option<String>,
    }
}
