use crate::model::telegram;
use crate::service::{self, conversation_util::record_message};
use actix_web::{
    get, post,
    web::{self, Json, ServiceConfig},
    HttpResponse, Responder,
};
use diesel::r2d2::{self, ConnectionManager};
use diesel::SqliteConnection;

pub type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

#[get("/health")]
async fn health() -> impl Responder {
    return HttpResponse::Ok().body("¯\\_(ツ)_/¯\n");
}

#[post("/update")]
async fn get_update(update: Json<telegram::Update>, db: web::Data<Pool>) -> impl Responder {
    // todo validate if private chat

    record_message(&mut db.get().unwrap(), update.message.clone());
    if let Some(message) = update.message.clone() {
        let mut message_connection = db.get().unwrap();
        actix_web::rt::spawn(async move {
            let response = service::parse_message(&mut message_connection, &message).await;
            record_message(&mut message_connection, response.ok());
        });
    }
    if let Some(callback_query) = update.callback_query.clone() {
        let mut callback_connection = db.get().unwrap();
        actix_web::rt::spawn(async move {
            let response = service::parse_callback(&mut callback_connection, &callback_query).await;
            record_message(&mut callback_connection, response.ok());
        });
    }
    return HttpResponse::Ok();
}

pub fn config_api(app: &mut ServiceConfig) {
    app.service(get_update);
    app.service(health);
}
