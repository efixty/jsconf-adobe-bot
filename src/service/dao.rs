use crate::model::{
    db::{IdContainer, ScoreContainer},
    telegram::send::InlineKeyboardButton,
};

use log::info;

use super::*;

pub fn get_new_question(
    conn: &mut Conn,
    user_id: i64,
    quiz_id: i64,
    topic_id: i64,
) -> db::QuizQuestion {
    return diesel::dsl::sql_query(
        "
        select *
        from   quiz_question qq
        where  qq.id not in (select qm.quiz_question_id
                            FROM   quiz_message qm
                            WHERE  qm.user_id = ?
                                and qm.quiz_id = ?)
                and qq.topic_id = ?
        order by random()
        limit  1",
    )
    .bind::<diesel::sql_types::BigInt, i64>(user_id)
    .bind::<diesel::sql_types::BigInt, i64>(quiz_id)
    .bind::<diesel::sql_types::BigInt, i64>(topic_id)
    .get_result::<db::QuizQuestion>(conn)
    .expect("error getting new question");
}

pub fn get_answer_options(conn: &mut Conn, question_id: i64) -> Vec<db::AnswerOption> {
    return answer_option::dsl::answer_option
        .filter(answer_option::dsl::question_id.eq(question_id))
        .get_results(conn)
        .expect("error getting answer options");
}

pub fn get_question(conn: &mut Conn, question_id: i64) -> internal::Question {
    let question: Vec<(db::QuizQuestion, db::AnswerOption)> = quiz_question::table
        .filter(quiz_question::dsl::id.eq(question_id))
        .inner_join(answer_option::table)
        .load::<(db::QuizQuestion, db::AnswerOption)>(conn)
        .unwrap();

    return internal::Question {
        id: question_id,
        question_text: String::from(&question[0].0.question_text),
        answer_options: question
            .iter()
            .map(|(_, option)| internal::AnswerOption {
                id: option.id,
                option_text: String::from(&option.option_text),
            })
            .collect::<Vec<internal::AnswerOption>>(),
        correct_answer_option_id: question[0].0.correct_answer_option_id,
    };
}

pub fn get_user(conn: &mut Conn, user_id: i64) -> Option<db::User> {
    return users::dsl::users
        .filter(users::dsl::id.eq(user_id))
        .get_result::<db::User>(conn)
        .ok();
}

pub fn get_quiz(conn: &mut Conn, quiz_id: i64) -> Option<db::Quiz> {
    return quiz::dsl::quiz.find(quiz_id).first::<db::Quiz>(conn).ok();
}

pub fn get_topic(conn: &mut Conn, topic_id: i64) -> internal::Topic {
    return topic::dsl::topic
        .filter(topic::dsl::id.eq(topic_id))
        .get_result::<db::Topic>(conn)
        .map(internal::Topic::from)
        .expect("topic not found");
}

pub fn get_topics(conn: &mut Conn) -> Vec<internal::Topic> {
    return topic::dsl::topic
        .load::<db::Topic>(conn)
        .expect("Problem loading topics")
        .into_iter()
        .map(internal::Topic::from)
        .collect::<Vec<internal::Topic>>();
}

pub fn create_quiz(conn: &mut Conn, callback: &telegram::CallbackQuery) -> db::Quiz {
    let topic_id = callback.data[1..]
        .parse::<i64>()
        .expect("callback data invalidity");
    let new_quiz = db::NewQuiz {
        topic_id,
        user_id: callback.from.id,
    };
    diesel::insert_into(quiz::table)
        .values(new_quiz)
        .execute(conn);
    diesel::sql_query("select * from quiz where id = (select last_insert_rowid());")
        .get_result::<db::Quiz>(conn)
        .expect("error getting newly inserted row")
}

pub fn save_question_answer(conn: &mut Conn, quiz_answer: db::QuizMessage) {
    diesel::insert_into(quiz_message::table)
        .values(&quiz_answer)
        .execute(conn)
        .expect("couldn't save quiz question answer");
}

pub fn get_answered_questions(conn: &mut Conn, user_id: i64, quiz_id: i64) -> Vec<db::QuizMessage> {
    return quiz_message::dsl::quiz_message
        .filter(quiz_message::dsl::user_id.eq(user_id))
        .filter(quiz_message::dsl::quiz_id.eq(quiz_id))
        .get_results::<db::QuizMessage>(conn)
        .expect("error getting answered questions");
}

pub fn abort_quiz(conn: &mut Conn, user_id: i64) {
    let quiz_id = users::dsl::users
        .filter(users::dsl::id.eq(user_id))
        .get_result::<db::User>(conn)
        .expect("user not found")
        .last_quiz_id
        .expect("what are you trying to abort then?????");
    diesel::update(quiz::dsl::quiz)
        .filter(quiz::dsl::id.eq(quiz_id))
        .set(quiz::dsl::aborted.eq(true))
        .execute(conn);
}

pub fn finalize_quiz(conn: &mut Conn, quiz_id: i64) {
    diesel::update(quiz::dsl::quiz)
        .filter(quiz::dsl::id.eq(quiz_id))
        .set(quiz::dsl::finished.eq(true))
        .execute(conn);
}

pub fn update_user_last_quiz(conn: &mut Conn, user_id: i64, quiz_id: i64) {
    diesel::update(users::dsl::users)
        .filter(users::dsl::id.eq(user_id))
        .set(users::dsl::last_quiz_id.eq(quiz_id))
        .execute(conn);
}
