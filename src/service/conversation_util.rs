use crate::{schema::*, model::telegram::send::EditMessage};

use super::{handlers::callback_handlers, *};
use handlers::command_handlers;
use super::*;

use log::info;

const PHRASES: [&str; 3] = ["stop wasting my time 😤", "42", "functional programming!!!"];

pub async fn parse_command(
    conn: &mut Conn,
    message: &telegram::Message,
) -> Result<telegram::Message> {
    match &message.text[..] {
        "/start" => command_handlers::initiate_conversation(conn, message).await,
        "/quiz" => command_handlers::handle_quiz(conn, message).await,
        // todo: support /help
        _ => telegram_api::send_plain_text(message.chat.id, String::from("unknow command")).await,
    }
}

pub async fn parse_callback(
    conn: &mut Conn,
    callback: &telegram::CallbackQuery,
) -> Result<telegram::Message> {
    match &callback.data[..1] {
        "a" => {
            return callback_handlers::handle_ongoing_quiz_abort_callback(conn, &callback).await;
            // get last quiz id from user
            // set aborted flag as true
            // send topic selection message
        }
        "t" => {
            return callback_handlers::handle_choose_topic_callback(conn, &callback).await;
            // let topic_id = callback.data[1..].parse::<i64>().expect("callback data incorrect");
            // prepare_quiestion(conn, callback.from.id, topic_id);
        }
        "q" => {
            return callback_handlers::handle_quiz_question_answer_callback(conn, &callback).await;
            // let tokenized_callback = callback.data[1..].split("|").map(|id| id.parse::<i64>()).collect::<Vec<i64>>();
            // let (question_id, answer_option_id) = (tokenized_callback[0], tokenized_callback[1]);
            // prepare_question(conn, callback.from.id, topic_id);
        }
        _ => {
            log::debug!("got unknown callback query: {}", callback.data);
            return telegram_api::send_plain_text(
                callback.message.chat.id,
                "something went terribly wrong, contact @efixty".to_string(),
            )
            .await
        }
    };
}

pub async fn send_quiz_abort_suggestion(chat_id: i64) -> Result<telegram::Message> {
    let quiz_abort_suggestion_message = telegram::send::Message {
        chat_id,
        text: String::from("You have an ongoing quiz\nTo start a new quiz you will have to either abort or finish this one"),
        parse_mode: None,
        reply_to_message_id: None, // replace with the last quiz message's id
        reply_markup: Some(telegram::send::InlineKeyboardMarkup::quiz_abortion_reply_markup()),
    };
    return telegram_api::send_custom_message(quiz_abort_suggestion_message).await;
}

pub fn record_message(conn: &mut Conn, message: Option<telegram::Message>) {
    if let Some(new_message) = message {
        let new_message = db::NewMessage::from((&new_message, new_message.from.is_bot));
        diesel::insert_into(message::dsl::message)
            .values(&new_message)
            .execute(conn)
            .expect(&format!("error saving new message {:?}", new_message));
    }
}

pub fn record_answer(conn: &mut Conn, message_id: i64, user_id: i64, answer: &internal::AnswerCallbackPayload) {
    let quiz_answer = db::QuizMessage {
        message_id,
        answer: answer.answer_id,
        quiz_question_id: answer.question_id,
        quiz_id: answer.quiz_id,
        user_id,
    };
    dao::save_question_answer(conn, quiz_answer);
}

pub async fn edit_answered_quiz_message(conn: &mut Conn, message: &telegram::Message, answer: &internal::AnswerCallbackPayload) {
    let question = dao::get_question(conn, answer.question_id);
    let edit_message_payload = telegram::send::EditMessage {
        message_id: message.message_id,
        chat_id: message.chat.id,
        text: format!(
            "{}\n\n{}\nYour answer is <b>{}</b>",
            question.question_text,
            question
                .answer_options
                .iter()
                .enumerate()
                .map(|(index, option)| format!("{}. {}\n", index + 1, option.option_text))
                .collect::<String>(),
            answer.answer_option_index
        ),
        parse_mode: Some(telegram::ParseMode::Html.value()),
    };
    telegram_api::edit_message(edit_message_payload).await;
}

#[inline(always)]
pub fn random_answer() -> String {
    let answer = PHRASES[rand::random::<i64>().rem_euclid(PHRASES.len() as i64) as usize];
    return String::from(answer);
}

pub fn prepare_next_question(conn: &mut Conn, user_id: i64, topic_id: i64, quiz_id: i64) -> internal::QuizQuestion {
    let quiz_question = dao::get_new_question(conn, user_id, quiz_id, topic_id);
    info!("answered questions are {:?}", dao::get_answered_questions(conn, user_id, quiz_id));
    let answer_options = dao::get_answer_options(conn, quiz_question.id);
    let reply_markup_buttons = answer_options
        .iter()
        .enumerate()
        .map(|(index, option_entity)| {
            return telegram::send::InlineKeyboardButton {
                text: format!("{}", index + 1),
                callback_data: format!("q{}|{}|{}|{}|{}", quiz_question.id, option_entity.id, topic_id, quiz_id, index + 1),
            };
        })
        .collect::<Vec<telegram::send::InlineKeyboardButton>>();
    let mut question_full_text = quiz_question.question_text.clone();
    question_full_text.push_str("\n\n");
    question_full_text.push_str(
        &answer_options
            .iter()
            .enumerate()
            .map(|(index, option)| format!("{}. {}\n", index + 1, option.option_text))
            .collect::<String>(),
    );
    return internal::QuizQuestion {
        question: question_full_text,
        options_buttons: reply_markup_buttons,
    };
}

pub fn calculate_score(conn: &mut Conn, user_id: i64, quiz_id: i64) -> internal::QuizCompletion {
    let score: i32 = diesel::sql_query(
        "
        select count(*) as score
        from   quiz_message qm
               inner join quiz_question qq
                       on qq.id = qm.quiz_question_id
               inner join answer_option ao
                       on qq.correct_answer_option_id = ao.id
                          and qm.answer = ao.id
        where  qm.user_id = ?
                and qm.quiz_id = ?
        ",
    )
    .bind::<diesel::sql_types::BigInt, i64>(user_id)
    .bind::<diesel::sql_types::BigInt, i64>(quiz_id)
    .get_result::<db::ScoreContainer>(conn)
    .expect("failed to calculate the score")
    .score;

    return internal::QuizCompletion::from(score as i8);
}

pub async fn abort_quiz(conn: &mut Conn, answer_payload: &CallbackQuery) {
    dao::abort_quiz(conn, answer_payload.from.id);
    let abortion_suggestion_edit = EditMessage {
        chat_id: answer_payload.message.chat.id,
        message_id: answer_payload.message.message_id,
        text: String::from("You aborted the ongoing quiz🥲"),
        parse_mode: None,
    };
    telegram_api::edit_message(abortion_suggestion_edit).await;
}

pub async fn edit_topic_choose_message(conn: &mut Conn, choosen_topic_callback: &telegram::CallbackQuery) {
    let topic_id = choosen_topic_callback.data[1..]
    .parse::<i64>()
    .expect("callback data invalidity");
    let choosen_topic = dao::get_topic(conn, topic_id);
    let topic_choose_message_edit = telegram::send::EditMessage {
        chat_id: choosen_topic_callback.message.chat.id,
        message_id: choosen_topic_callback.message.message_id,
        text: format!("Starting <b>{}</b> quiz 🤓", choosen_topic.name),
        parse_mode: Some(telegram::ParseMode::Html.value()),
    };
    telegram_api::edit_message(topic_choose_message_edit).await;
}