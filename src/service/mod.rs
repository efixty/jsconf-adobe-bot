use crate::diesel::prelude::*;
use crate::model::telegram::CallbackQuery;
use crate::model::{db, internal, telegram, telegram::Result};
use crate::schema::{answer_option, message, quiz_message, quiz_question, users, quiz, topic};
use crate::telegram_api;
use diesel::r2d2::ConnectionManager;
use diesel::SqliteConnection;

pub type Pool = diesel::r2d2::Pool<ConnectionManager<SqliteConnection>>;
pub type Conn = diesel::r2d2::PooledConnection<ConnectionManager<SqliteConnection>>;

pub mod handlers;
pub mod conversation_util;
pub mod dao;

pub async fn parse_message(
    conn: &mut Conn,
    message: &telegram::Message,
) -> Result<telegram::Message> {
    if message.text.starts_with("/") {
        return conversation_util::parse_command(conn, &message).await;
    } else {
        return telegram_api::send_plain_text(message.chat.id, conversation_util::random_answer()).await;
    }
}

pub async fn parse_callback(conn: &mut Conn, callback: &telegram::CallbackQuery) -> Result<telegram::Message> {
    return conversation_util::parse_callback(conn, callback).await;
}