use super::*;

const QUIZ_QUESTION_COUNT: usize = 10;

pub mod command_handlers {
    use super::*;
    pub async fn initiate_conversation(
        conn: &mut Conn,
        message: &telegram::Message,
    ) -> Result<telegram::Message> {
        let user = dao::get_user(conn, message.from.id);
        if let Some(_) = user {
            return telegram_api::send_plain_text(message.chat.id, String::from("hi again")).await;
        }
        let new_user = db::User::from(message);
        diesel::insert_into(users::table)
            .values(&new_user)
            .execute(conn)
            .expect("error creating user");
        return telegram_api::send_plain_text(
            message.chat.id,
            format!(
                "Hi {}, I'm Adobe Bot\nLet's play a game 😈\nPress 👉 /quiz",
                new_user.first_name
            ),
        )
        .await;
    }

    pub async fn handle_quiz(
        conn: &mut Conn,
        message: &telegram::Message,
    ) -> Result<telegram::Message> {
        let user = dao::get_user(conn, message.from.id);
        if let None = user {
            return telegram_api::send_plain_text(message.chat.id, String::from("👉 /start")).await;
        }
        let ongoing_quiz = user
            .and_then(|u| u.last_quiz_id)
            .and_then(|quiz_id| dao::get_quiz(conn, quiz_id))
            .filter(|quiz| !quiz.aborted && !quiz.finished);
        return match ongoing_quiz {
            Some(_) => conversation_util::send_quiz_abort_suggestion(message.chat.id).await,
            None => telegram_api::send_quiz_topics(message.chat.id, dao::get_topics(conn)).await,
        };
    }
}

pub mod callback_handlers {

    use super::*;

    pub async fn handle_choose_topic_callback(
        conn: &mut Conn,
        callback: &telegram::CallbackQuery,
    ) -> Result<telegram::Message> {
        let new_quiz = dao::create_quiz(conn, &callback);
        dao::update_user_last_quiz(conn, callback.from.id, new_quiz.id);
        conversation_util::edit_topic_choose_message(conn, callback).await;
        let new_question = conversation_util::prepare_next_question(
            conn,
            callback.from.id,
            new_quiz.topic_id,
            new_quiz.id,
        );
        return telegram_api::send_quiz_question(callback.message.chat.id, new_question).await;
    }

    pub async fn handle_quiz_question_answer_callback(
        conn: &mut Conn,
        answer_payload: &telegram::CallbackQuery,
    ) -> Result<telegram::Message> {
        let answer = internal::AnswerCallbackPayload::from(String::from(&answer_payload.data[1..]));
        conversation_util::record_answer(
            conn,
            answer_payload.message.message_id,
            answer_payload.from.id,
            &answer,
        );
        conversation_util::edit_answered_quiz_message(conn, &answer_payload.message, &answer).await;

        let answered_questions: Vec<db::QuizMessage> =
            dao::get_answered_questions(conn, answer_payload.from.id, answer.quiz_id);

        if answered_questions.len() < QUIZ_QUESTION_COUNT {
            let new_quiz_question = conversation_util::prepare_next_question(
                conn,
                answer_payload.from.id,
                answer.topic_id,
                answer.quiz_id,
            );
            return telegram_api::send_quiz_question(
                answer_payload.message.chat.id,
                new_quiz_question,
            )
            .await;
        } else {
            dao::finalize_quiz(conn, answer.quiz_id);
            let score_payload =
                conversation_util::calculate_score(conn, answer_payload.from.id, answer.quiz_id);
            return telegram_api::send_score_message(answer_payload.message.chat.id, score_payload)
                .await;
        }
    }

    pub async fn handle_ongoing_quiz_abort_callback(
        conn: &mut Conn,
        answer_payload: &telegram::CallbackQuery,
    ) -> Result<telegram::Message> {
        conversation_util::abort_quiz(conn, answer_payload).await;
        return telegram_api::send_quiz_topics(
            answer_payload.message.chat.id,
            dao::get_topics(conn),
        )
        .await;
    }
}
