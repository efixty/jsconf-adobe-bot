#[macro_use]
extern crate diesel;

use actix_web::{
    middleware,
    web::{self, Data},
    App, HttpServer,
};
use diesel::r2d2::{self, ConnectionManager};
use diesel::SqliteConnection;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

pub mod model;
pub mod route;
pub mod schema;
pub mod service;
pub mod telegram_api;

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

pub type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    init_logger();
    let pool = init_sqlite_pool();
    execute_db_migrations(&mut pool.get().unwrap());
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(pool.clone()))
            .wrap(middleware::Logger::default())
            .service(web::scope("/api").configure(route::config_api))
    })
    .bind(("0.0.0.0", 7143))?
    .run()
    .await
}

fn init_logger() {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("WARN"));
}

fn init_sqlite_pool() -> Pool {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let connection_manager = ConnectionManager::<SqliteConnection>::new(database_url);
    return r2d2::Pool::builder()
        .build(connection_manager)
        .expect("failed to create pool");
}

fn execute_db_migrations(conn: &mut impl MigrationHarness<diesel::sqlite::Sqlite>) {
    conn.run_pending_migrations(MIGRATIONS)
        .expect("migrations failed to run");
}
