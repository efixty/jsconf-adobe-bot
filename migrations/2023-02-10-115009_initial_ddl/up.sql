create table users (
    id integer primary key,
    chat_id bigint not null,
    first_name varchar not null,
    last_name varchar,
    username varchar,
    last_quiz_id bigint
);
create table message (
    id integer primary key autoincrement,
    message_id bigint not null,
    user_id bigint not null,
    text varchar not null,
    incoming boolean not null
);
create table quiz_message (
    message_id bigint not null,
    user_id bigint not null,
    quiz_question_id bigint not null,
    answer varchar,
    quiz_id bigint not null
);
create table answer_option (
    id bigint primary key,
    option_text varchar not null,
    question_id bigint not null
);
create table quiz (
    id integer primary key autoincrement,
    user_id bigint not null,
    topic_id bigint not null,
    aborted boolean not null default false,
    finished boolean not null default false
);
create table quiz_question (
    id integer primary key,
    question_text varchar not null,
    correct_answer_option_id bigint not null,
    topic_id bigint not null default 1
);
create table topic (
    id integer primary key autoincrement,
    name varchar not null
);