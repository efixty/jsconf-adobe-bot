-------------- question 1 ------------------------------------------------------------
delete from quiz_question where id = 1;
delete from answer_option where id in (1, 2, 3, 4);

insert into quiz_question (id, question_text, correct_answer_option_id)
values (1, "What is the output of this👇 code?" || x'0a' || x'0a' ||
"<pre><code>" ||
"let name = 'John';" || x'0a' ||
"function sayHi() {" || x'0a' ||
"  alert(name);" || x'0a' ||
"}" || x'0a' ||
"setTimeout(function() {" || x'0a' ||
"  let name = 'Lucy';" || x'0a' ||
"  sayHi();" || x'0a' ||
"}, 1000);" || "</code></pre>", 1);
insert into answer_option
values (1, "John", 1);
insert into answer_option
values (2, "Lucy", 1);
insert into answer_option
values (3, "<code>undefined</code>", 1);
insert into answer_option
values (
        4,
        "error",
        1
    );
-------------- question 2 ------------------------------------------------------------
delete from quiz_question where id = 2;
delete from answer_option where id = 5;
delete from answer_option where id = 6;
delete from answer_option where id = 7;

insert into quiz_question (id, question_text, correct_answer_option_id)
values (2, 
    "What is the output of this👇 code?" || x'0a' || x'0a' ||
    "<pre><code>" ||
    "let a = 1;" || x'0a' ||
    "let b = { toString() {return '1'} };" || x'0a' ||
    "let c = 1;" ||  x'0a' ||
    "console.log(a + b + c);" || "</code></pre>",
6);
insert into answer_option
values (5, "11[objcet Object]", 2);
insert into answer_option
values (6, "111", 2);
insert into answer_option
values (7, "3", 2);

-------------- question 3 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id)
values (
        3,
        "What is the output of this👇 code?" || x'0a' || x'0a' ||
        "<pre><code>" ||
        "console.log(a);" || x'0a' ||
        "var a = 10;" ||
        "</code></pre>",
        10
);
insert into answer_option values (8, "10", 3);
insert into answer_option values (9, "ReferenceError", 3);
insert into answer_option values (10, "undefined", 3);
insert into answer_option values (11, "Compilation Error", 3);

-------------- question 4 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id)
values (
    4,
    "What is the output of this👇 code?" || x'0a' || x'0a' ||
    "When creating a <code>Number</code> object, what do passed values that can't be converted return?",
    13
);
insert into answer_option values (12, "isNaN", 4);
insert into answer_option values (13, "NaN", 4);
insert into answer_option values (14, "null", 4);
insert into answer_option values (15, "undefined", 4);

-------------- question 5 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id)
values (
    5,
    "What will be the output of this👇 code?" || x'0a' || x'0a' ||
    "<pre><code>" ||
    "var x = 3;" || x'0a' ||
    "var foo = {" || x'0a' ||
    "    x: 2," || x'0a' ||
    "    bar: {" || x'0a' ||
    "        x: 1," || x'0a' ||
    "        baz: function () {" || x'0a' ||
    "            return this.x;" || x'0a' ||
    "        }" || x'0a' ||
    "    }" || x'0a' ||
    "}" || x'0a' || x'0a' ||
    "var go = foo.bar.baz;" || x'0a' ||
    "console.log(`${go()},${foo.bar.baz()}`);" ||
    "</code></pre>",
    19
);
insert into answer_option values (16, "<pre><code>1, 2</code></pre>", 5);
insert into answer_option values (17, "<pre><code>1, 3</code></pre>", 5);
insert into answer_option values (18, "<pre><code>2, 1</code></pre>", 5);
insert into answer_option values (19, "<pre><code>3, 1</code></pre>", 5);

-------------- question 6 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id) 
values(
    6,
    "Which of the following is an event in JavaScript?" || x'0a' || x'0a',
    23
);

insert into answer_option values (20, "A page loading", 6);
insert into answer_option values (21, "The user clicking an element", 6);
insert into answer_option values (22, "The user resizing a window", 6);
insert into answer_option values (23, "All of the above", 6);

-------------- question 7 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id) 
values(
    7,
    "The Java Script statement <code>a = new Array(2,4)</code>?" || x'0a' || x'0a',
    26
);

insert into answer_option values (24, "Defines a new two-dimensional array a whose dimensions are 2 and 4", 7);
insert into answer_option values (25, "Defines an array a and assigns the values 2 and 4 to a[1] and a[2]", 7);
insert into answer_option values (26, "Defines an array a and assigns the values 2 and 4 to a[0] and a[1]", 7);
insert into answer_option values (27, "Defines a three-element array whose elements have indexes 2 through 4", 7);

-------------- question 8 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id) 
values(
    8,
    "Which of the following JavaScript statements is correct definition of an array?" || x'0a' || x'0a',
    28
);

insert into answer_option values (28, "<code>var a = new Array(100)</code>", 8);
insert into answer_option values (29, "<code>var a = new Array[100]</code>", 8);
insert into answer_option values (30, "<code>var a = new String[100]</code>", 8);
insert into answer_option values (31, "<code>a = new Arrayl100]</code>", 8);

-------------- question 9 ------------------------------------------------------------
insert into quiz_question (id, question_text, correct_answer_option_id) 
values(
    9,
    "What does the following code return?" || x'0a' ||
    "<code>null == undefined</code>" || x'0a' || x'0a',
    33
);

insert into answer_option values (32, "<code>false</code>", 9);
insert into answer_option values (33, "<code>true</code>", 9);
insert into answer_option values (34, "Error in the code", 9);

-------------- question 10 ------------------------------------------------------------

delete from quiz_question where id = 10;
delete from answer_option where id = 35;
delete from answer_option where id = 36;
delete from answer_option where id = 37;
delete from answer_option where id = 38;

insert into quiz_question (id, question_text, correct_answer_option_id) 
values(
    10,
    "What will be the output of the 👇 code?" || x'0a' || x'0a' ||
    "<pre><code>" ||
    "var i = 19;" || x'0a' ||
    "var j = 7;" || x'0a' ||
    "var do_you_like_books = i+++-j--+i++-j--+i-j;" || x'0a' ||
    "console.log(do_you_like_books);" ||
    "</code></pre>",
    37
);

insert into answer_option values (35, "<code>39</code>", 10);
insert into answer_option values (36, "<code>54</code>", 10);
insert into answer_option values (37, "<code>42</code>", 10);
insert into answer_option values (38, "<code>-12</code>", 10);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (19, "What is the output of the following code: <code>console.log(typeof undefined === typeof null);</code>?", 71, 1);
insert into answer_option values (71, "true", 19);
insert into answer_option values (72, "false", 19);
insert into answer_option values (73, "undefined", 19);
insert into answer_option values (74, "null", 19);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (20, "What is the output of the following code: <code>console.log(+'20' + + '20');</code>?", 76, 1);
insert into answer_option values (75, "2020", 20);
insert into answer_option values (76, "40", 20);
insert into answer_option values (77, "NaN", 20);
insert into answer_option values (78, "undefined", 20);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (21, "What is the output of the following code: <code>console.log('1' + 2 + 3 + 4);</code>?", 80, 1);
insert into answer_option values (79, "10", 21);
insert into answer_option values (80, "1234", 21);
insert into answer_option values (81, "NaN", 21);
insert into answer_option values (82, "undefined", 21);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (22, "What is the output of the following code: <code>console.log([1, 2, 3] + [4, 5, 6]);</code>?", 83, 1);
insert into answer_option values (83, "1,2,34,5,6", 22);
insert into answer_option values (84, "123456", 22);
insert into answer_option values (85, "NaN", 22);
insert into answer_option values (86, "undefined", 22);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (23, "What is the output of the following code: <code>console.log('hello'.indexOf('l'));</code>?", 88, 1);
insert into answer_option values (87, "0", 23);
insert into answer_option values (88, "2", 23);
insert into answer_option values (89, "3", 23);
insert into answer_option values (90, "-1", 23);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (24, "What is the output of the following code: <code>console.log('hello'.charAt(2));</code>?", 93, 1);
insert into answer_option values (91, "h", 24);
insert into answer_option values (92, "e", 24);
insert into answer_option values (93, "l", 24);
insert into answer_option values (94, "o", 24);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (25, "What is the output of the following code: <code>console.log('hello'.length);</code>?", 96, 1);
insert into answer_option values (95, "4", 25);
insert into answer_option values (96, "5", 25);
insert into answer_option values (97, "6", 25);
insert into answer_option values (98, "undefined", 25);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (26, "What is the output of the following code: <code>console.log(typeof NaN);</code>?", 99, 1);
insert into answer_option values (99, "number", 26);
insert into answer_option values (100, "string", 26);
insert into answer_option values (101, "undefined", 26);
insert into answer_option values (102, "NaN", 26);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (27, "What is the output of the following code: <code>console.log(NaN === NaN);</code>?", 104, 1);
insert into answer_option values (103, "true", 27);
insert into answer_option values (104, "false", 27);
insert into answer_option values (105, "undefined", 27);
insert into answer_option values (106, "NaN", 27);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (28, "What is the output of the following code: <code>console.log('Hello World!'.toLowerCase());</code>?", 109, 1);
insert into answer_option values (107, "HELLO WORLD!", 28);
insert into answer_option values (108, "Hello World!", 28);
insert into answer_option values (109, "hello world!", 28);
insert into answer_option values (110, "undefined", 28);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (29, "What is the output of the following code: <code>console.log('hello world'.replace('world', 'everyone'));</code>?", 111, 1);
insert into answer_option values (111, "hello everyone", 29);
insert into answer_option values (112, "everyone world", 29);
insert into answer_option values (113, "world everyone", 29);
insert into answer_option values (114, "undefined", 29);

