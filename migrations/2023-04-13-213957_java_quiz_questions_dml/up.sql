insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (30, "What is the output of the following code?" || x'0a' || 
"" || x'0a' || 
"Integer i1 = 1000;" || x'0a' || 
"Integer i2 = 1000;" || x'0a' || 
"System.out.println(i1 == i2);" || x'0a' || 
"", 116, 2);
insert into answer_option values (115, "true", 30);
insert into answer_option values (116, "false", 30);
insert into answer_option values (117, "It depends on the implementation of the JVM", 30);
insert into answer_option values (118, "Compile error", 30);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (31, "Which of the following is true about the 'transient' keyword in Java?", 119, 2);
insert into answer_option values (119, "It is used to indicate that a variable should not be serialized", 31);
insert into answer_option values (120, "It is used to indicate that a variable should be given a default value", 31);
insert into answer_option values (121, "It is used to indicate that a variable should be shared between all instances of a class", 31);
insert into answer_option values (122, "It is used to indicate that a variable should be marked as final", 31);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (32, "What is the output of the following code?" || x'0a' || 
"" || x'0a' || 
"String s1 = ""hello"";" || x'0a' || 
"String s2 = ""hello"";" || x'0a' || 
"System.out.println(s1 == s2);" || x'0a' || 
"", 123, 2);
insert into answer_option values (123, "true", 32);
insert into answer_option values (124, "false", 32);
insert into answer_option values (125, "It depends on the implementation of the JVM", 32);
insert into answer_option values (126, "Compile error", 32);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (33, "What is the access level of a constructor in Java?", 127, 2);
insert into answer_option values (127, "public", 33);
insert into answer_option values (128, "private", 33);
insert into answer_option values (129, "protected", 33);
insert into answer_option values (130, "package-private", 33);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (34, "Which of the following is NOT a type of exception in Java?", 134, 2);
insert into answer_option values (131, "Checked exceptions", 34);
insert into answer_option values (132, "Unchecked exceptions", 34);
insert into answer_option values (133, "Error exceptions", 34);
insert into answer_option values (134, "Fatal exceptions", 34);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (35, "What is the output of the following code?" || x'0a' || 
"" || x'0a' || 
"int x = 5;" || x'0a' || 
"int y = x++ + x--;" || x'0a' || 
"System.out.println(y);" || x'0a' || 
"", 137, 2);
insert into answer_option values (135, "9", 35);
insert into answer_option values (136, "10", 35);
insert into answer_option values (137, "11", 35);
insert into answer_option values (138, "12", 35);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (36, "Which of the following is NOT a valid declaration of a 'main' method in Java?", 141, 2);
insert into answer_option values (139, "public static void main(String[] args)", 36);
insert into answer_option values (140, "public static void main(String args[])", 36);
insert into answer_option values (141, "public void main(String[] args)", 36);
insert into answer_option values (142, "public static void main(String... args)", 36);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (37, "What is the output of the following code?" || x'0a' || 
"" || x'0a' || 
"int a = 2;" || x'0a' || 
"int b = 5;" || x'0a' || 
"boolean c = (a == 2) && (b++ == 5);" || x'0a' || 
"System.out.println(b);" || x'0a' || 
"", 144, 2);
insert into answer_option values (143, "4", 37);
insert into answer_option values (144, "5", 37);
insert into answer_option values (145, "6", 37);
insert into answer_option values (146, "7", 37);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (38, "Which of the following is NOT a feature of Java 8?", 149, 2);
insert into answer_option values (147, "Lambda expressions", 38);
insert into answer_option values (148, "Default methods in interfaces", 38);
insert into answer_option values (149, "The 'var' keyword", 38);
insert into answer_option values (150, "Streams", 38);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (39, "What is the purpose of the Java Virtual Machine (JVM)?", 152, 2);
insert into answer_option values (151, "To compile Java code", 39);
insert into answer_option values (152, "To execute Java code", 39);
insert into answer_option values (153, "To debug Java code", 39);
insert into answer_option values (154, "To write Java code", 39);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (40, "What is the keyword used to declare a class in Java?", 155, 2);
insert into answer_option values (155, "class", 40);
insert into answer_option values (156, "public", 40);
insert into answer_option values (157, "static", 40);
insert into answer_option values (158, "void", 40);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (41, "What is the difference between a constructor and a method?", 161, 2);
insert into answer_option values (159, "A constructor has a return type, a method does not", 41);
insert into answer_option values (160, "A constructor has a name, a method does not", 41);
insert into answer_option values (161, "A constructor is used to create objects, a method is used to perform operations on objects", 41);
insert into answer_option values (162, "A constructor can be called explicitly, a method cannot", 41);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (42, "What is a package in Java?", 163, 2);
insert into answer_option values (163, "A container for classes and interfaces", 42);
insert into answer_option values (164, "A type of variable", 42);
insert into answer_option values (165, "A type of loop", 42);
insert into answer_option values (166, "A type of data structure", 42);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (43, "What is an abstract class in Java?", 170, 2);
insert into answer_option values (167, "A class that cannot be instantiated", 43);
insert into answer_option values (168, "A class that has no methods", 43);
insert into answer_option values (169, "A class that can only be accessed by other classes in the same package", 43);
insert into answer_option values (170, "A class that is used to define common behavior for a group of subclasses", 43);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (44, "What is the difference between an abstract class and an interface?", 171, 2);
insert into answer_option values (171, "An abstract class can have concrete methods, an interface cannot", 44);
insert into answer_option values (172, "An abstract class can be instantiated, an interface cannot", 44);
insert into answer_option values (173, "An abstract class can extend another class or implement multiple interfaces, an interface can only extend multiple interfaces", 44);
insert into answer_option values (174, "There is no difference between an abstract class and an interface", 44);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (45, "What is the difference between a checked exception and an unchecked exception?", 175, 2);
insert into answer_option values (175, "A checked exception must be caught or declared, an unchecked exception does not", 45);
insert into answer_option values (176, "A checked exception is thrown by the Java Virtual Machine, an unchecked exception is thrown by user code", 45);
insert into answer_option values (177, "A checked exception is a subclass of RuntimeException, an unchecked exception is not", 45);
insert into answer_option values (178, "There is no difference between a checked exception and an unchecked exception", 45);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (46, "What is a lambda expression in Java?", 182, 2);
insert into answer_option values (179, "A type of variable", 46);
insert into answer_option values (180, "A type of loop", 46);
insert into answer_option values (181, "A type of data structure", 46);
insert into answer_option values (182, "A way to express functional interfaces more concisely", 46);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (47, "What is the difference between an abstract class and an interface?", 184, 2);
insert into answer_option values (183, "An abstract class can have constructor while an interface cannot.", 47);
insert into answer_option values (184, "An interface can have default methods while an abstract class cannot.", 47);
insert into answer_option values (185, "An abstract class can have public, private, and protected methods while an interface can only have public methods.", 47);
insert into answer_option values (186, "All of the above.", 47);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (48, "How do you declare a constant in Java?", 188, 2);
insert into answer_option values (187, "By using the const keyword", 48);
insert into answer_option values (188, "By using the final keyword", 48);
insert into answer_option values (189, "By using the static keyword", 48);
insert into answer_option values (190, "By using the readonly keyword", 48);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (49, "How can you prevent a class from being inherited by other classes?", 191, 2);
insert into answer_option values (191, "By declaring the class as final", 49);
insert into answer_option values (192, "By declaring the class as static", 49);
insert into answer_option values (193, "By declaring the class as abstract", 49);
insert into answer_option values (194, "By declaring the class as private", 49);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (50, "What is polymorphism in Java and how is it implemented?", 195, 2);
insert into answer_option values (195, "Polymorphism is the ability of an object to take on many forms and it is implemented through method overloading and method overriding.", 50);
insert into answer_option values (196, "Polymorphism is the ability of an object to take on many forms and it is implemented through inheritance and encapsulation.", 50);
insert into answer_option values (197, "Polymorphism is the ability of an object to take on many forms and it is implemented through interface and abstraction.", 50);
insert into answer_option values (198, "None of the above.", 50);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (51, "What is the difference between a checked and an unchecked exception?", 199, 2);
insert into answer_option values (199, "Checked exceptions are checked at compile-time while unchecked exceptions are checked at runtime.", 51);
insert into answer_option values (200, "Checked exceptions are checked at runtime while unchecked exceptions are checked at compile-time.", 51);
insert into answer_option values (201, "Checked exceptions are subclass of RuntimeException while unchecked exceptions are not.", 51);
insert into answer_option values (202, "Unchecked exceptions are subclass of Exception while checked exceptions are not.", 51);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (52, "How do you declare and initialize an array in Java?", 205, 2);
insert into answer_option values (203, "int[] numbers = {1, 2, 3};", 52);
insert into answer_option values (204, "int[] numbers = new int[3] {1, 2, 3};", 52);
insert into answer_option values (205, "int[] numbers = new int[]{1, 2, 3};", 52);
insert into answer_option values (206, "All of the above.", 52);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (53, "What is the difference between a private and a protected method in Java?", 207, 2);
insert into answer_option values (207, "A private method can only be accessed within the same class while a protected method can be accessed within the same class and its subclasses.", 53);
insert into answer_option values (208, "A private method can be accessed within the same class and its subclasses while a protected method can only be accessed within the same class.", 53);
insert into answer_option values (209, "A private method can only be accessed within the same package while a protected method can be accessed from any package.", 53);
insert into answer_option values (210, "None of the above.", 53);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (54, "What is the difference between an instance variable and a class variable in Java?", 213, 2);
insert into answer_option values (211, "An instance variable is declared with the static keyword while a class variable is not.", 54);
insert into answer_option values (212, "An instance variable is a variable that is declared inside a method while a class variable is declared outside a method.", 54);
insert into answer_option values (213, "An instance variable is a variable that is associated with an instance of a class while a class variable is associated with the class itself.", 54);
insert into answer_option values (214, "None of the above.", 54);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (55, "What is a static method in Java and how is it different from an instance method?", 215, 2);
insert into answer_option values (215, "A static method is a method that can be accessed without creating an instance of the class while an instance method can only be accessed through an instance of the class.", 55);
insert into answer_option values (216, "A static method is a method that can be accessed through an instance of the class while an instance method can only be accessed without creating an instance of the class.", 55);
insert into answer_option values (217, "A static method is a method that is declared with the final keyword while an instance method is not.", 55);
insert into answer_option values (218, "None of the above.", 55);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (56, "How do you implement multithreading in Java?", 221, 2);
insert into answer_option values (219, "By extending the Thread class and overriding its run() method.", 56);
insert into answer_option values (220, "By implementing the Runnable interface and passing an instance of the implementation to the Thread constructor.", 56);
insert into answer_option values (221, "Both A and B.", 56);
insert into answer_option values (222, "None of the above.", 56);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (57, "What is the purpose of the ""finally"" block in a try-catch statement?", 224, 2);
insert into answer_option values (223, "To handle the exception that was thrown.", 57);
insert into answer_option values (224, "To execute code regardless of whether an exception was thrown or not.", 57);
insert into answer_option values (225, "To define custom exceptions.", 57);
insert into answer_option values (226, "None of the above.", 57);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (58, "How do you implement an interface in a class in Java?", 228, 2);
insert into answer_option values (227, "By extending the interface and implementing its methods.", 58);
insert into answer_option values (228, "By implementing the interface and providing implementations for its methods.", 58);
insert into answer_option values (229, "Both A and B.", 58);
insert into answer_option values (230, "None of the above.", 58);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (59, "What is a constructor in Java and how is it different from a method?", 233, 2);
insert into answer_option values (231, "A constructor is a method that returns a value while a method does not.", 59);
insert into answer_option values (232, "A constructor is a method that has the same name as the class while a method can have any name.", 59);
insert into answer_option values (233, "A constructor is used to initialize an object while a method is used to perform some action on an object.", 59);
insert into answer_option values (234, "None of the above.", 59);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (60, "What is the difference between a HashSet and a TreeSet in Java?", 235, 2);
insert into answer_option values (235, "A HashSet is unordered while a TreeSet is ordered.", 60);
insert into answer_option values (236, "A HashSet is ordered while a TreeSet is unordered.", 60);
insert into answer_option values (237, "Both A and B.", 60);
insert into answer_option values (238, "None of the above.", 60);