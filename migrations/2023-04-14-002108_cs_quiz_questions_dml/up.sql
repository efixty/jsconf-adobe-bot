insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (11, "What is the most basic unit of a computer's memory?", 41, 3);
insert into answer_option values (39, "Byte", 11);
insert into answer_option values (40, "Nibble", 11);
insert into answer_option values (41, "Bit", 11);
insert into answer_option values (42, "Word", 11);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (12, "What is a binary search?", 43, 3);
insert into answer_option values (43, "A search algorithm that divides the search interval in half at every step", 12);
insert into answer_option values (44, "A search algorithm that uses a hash table to search for elements", 12);
insert into answer_option values (45, "A search algorithm that uses recursion to search for elements", 12);
insert into answer_option values (46, "A search algorithm that randomly selects elements to search for", 12);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (13, "What is the difference between a stack and a queue?", 47, 3);
insert into answer_option values (47, "A stack uses a LIFO (last in, first out) ordering, while a queue uses a FIFO (first in, first out) ordering", 13);
insert into answer_option values (48, "A stack uses a FIFO (first in, first out) ordering, while a queue uses a LIFO (last in, first out) ordering", 13);
insert into answer_option values (49, "A stack and a queue are the same thing", 13);
insert into answer_option values (50, "A stack and a queue are both used for sorting algorithms", 13);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (14, "What is the purpose of a compiler?", 51, 3);
insert into answer_option values (51, "To translate high-level programming code into machine code", 14);
insert into answer_option values (52, "To optimize the performance of a program", 14);
insert into answer_option values (53, "To test and debug code", 14);
insert into answer_option values (54, "To create a graphical user interface for a program", 14);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (15, "What is an algorithm?", 55, 3);
insert into answer_option values (55, "A set of instructions for solving a problem", 15);
insert into answer_option values (56, "A type of programming language", 15);
insert into answer_option values (57, "A type of hardware component", 15);
insert into answer_option values (58, "A type of database management system", 15);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (16, "What is the difference between a linked list and an array?", 59, 3);
insert into answer_option values (59, "An array has a fixed size, while a linked list can grow or shrink dynamically", 16);
insert into answer_option values (60, "A linked list has a fixed size, while an array can grow or shrink dynamically", 16);
insert into answer_option values (61, "An array and a linked list are the same thing", 16);
insert into answer_option values (62, "An array and a linked list are both used for searching algorithms", 16);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (17, "What is the purpose of an operating system?", 63, 3);
insert into answer_option values (63, "To manage computer hardware and software resources", 17);
insert into answer_option values (64, "To create graphical user interfaces for programs", 17);
insert into answer_option values (65, "To optimize the performance of a program", 17);
insert into answer_option values (66, "To translate high-level programming code into machine code", 17);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (18, "What is recursion?", 67, 3);
insert into answer_option values (67, "A function that calls itself", 18);
insert into answer_option values (68, "A type of loop", 18);
insert into answer_option values (69, "A type of data structure", 18);
insert into answer_option values (70, "A type of programming language", 18);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (61, "What is the difference between a process and a thread in operating system?", 239, 3);
insert into answer_option values (239, "A process is a program in execution while a thread is a lightweight process that can be executed concurrently with other threads within a process.", 61);
insert into answer_option values (240, "A process is a collection of threads while a thread is a collection of processes.", 61);
insert into answer_option values (241, "Both A and B.", 61);
insert into answer_option values (242, "None of the above.", 61);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (62, "What is Big O notation and how is it used to analyze the efficiency of algorithms?", 243, 3);
insert into answer_option values (243, "Big O notation is a mathematical notation that describes the upper bound of an algorithm's running time or space complexity.", 62);
insert into answer_option values (244, "Big O notation is a mathematical notation that describes the lower bound of an algorithm's running time or space complexity.", 62);
insert into answer_option values (245, "Big O notation is a mathematical notation that describes the exact running time or space complexity of an algorithm.", 62);
insert into answer_option values (246, "None of the above.", 62);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (63, "What is a closure in programming and why is it useful?", 248, 3);
insert into answer_option values (247, "A closure is a function that returns a value while another function is executing.", 63);
insert into answer_option values (248, "A closure is a function that is defined inside another function and has access to its outer function's variables.", 63);
insert into answer_option values (249, "A closure is a function that is defined outside of any other function and can be called from anywhere in the program.", 63);
insert into answer_option values (250, "None of the above.", 63);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (64, "What is the difference between a directed graph and an undirected graph?", 251, 3);
insert into answer_option values (251, "In an undirected graph, the edges do not have a direction while in a directed graph, the edges have a direction.", 64);
insert into answer_option values (252, "In an undirected graph, the edges have a direction while in a directed graph, the edges do not have a direction.", 64);
insert into answer_option values (253, "Both A and B.", 64);
insert into answer_option values (254, "None of the above.", 64);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (65, "What is dynamic programming and how is it used to solve problems?", 255, 3);
insert into answer_option values (255, "Dynamic programming is a method for solving problems by breaking them down into smaller subproblems and solving them recursively.", 65);
insert into answer_option values (256, "Dynamic programming is a method for solving problems by repeatedly trying all possible solutions until the correct one is found.", 65);
insert into answer_option values (257, "Dynamic programming is a method for solving problems by modeling them as a set of constraints and using a solver to find a solution that satisfies those constraints.", 65);
insert into answer_option values (258, "None of the above.", 65);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (66, "What is an AVL tree and how does it maintain balance?", 259, 3);
insert into answer_option values (259, "An AVL tree is a self-balancing binary search tree where the heights of the left and right subtrees of any node differ by at most one.", 66);
insert into answer_option values (260, "An AVL tree is a binary search tree where the left and right subtrees of any node have the same height.", 66);
insert into answer_option values (261, "An AVL tree is a binary search tree where the heights of the left and right subtrees of any node differ by at most two.", 66);
insert into answer_option values (262, "None of the above.", 66);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (67, "What is the difference between a semaphore and a mutex?", 263, 3);
insert into answer_option values (263, "A semaphore can be used to protect multiple resources while a mutex is used to protect a single resource.", 67);
insert into answer_option values (264, "A mutex can be used to protect multiple resources while a semaphore is used to protect a single resource.", 67);
insert into answer_option values (265, "Both A and B.", 67);
insert into answer_option values (266, "None of the above.", 67);

insert into quiz_question (id, question_text, correct_answer_option_id, topic_id) values (68, "What is the difference between a compiler and an interpreter?", 267, 3);
insert into answer_option values (267, "A compiler translates source code into machine code before the program is run while an interpreter translates and executes the source code line by line at runtime.", 68);
insert into answer_option values (268, "An interpreter translates source code into machine code before the program is run while a compiler translates and executes the source code line by line at runtime.", 68);
insert into answer_option values (269, "Both A and B.", 68);
insert into answer_option values (270, "None of the above.", 68);