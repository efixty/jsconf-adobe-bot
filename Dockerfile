####################################################################################################
## Builder #########################################################################################
####################################################################################################
FROM rust:1.67 AS builder

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y apt-utils

WORKDIR /jsconf-adobe-bot

COPY ./ .

RUN cargo build --release

####################################################################################################
## Final image #####################################################################################
####################################################################################################
FROM debian:11

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y libsqlite3-dev libssl-dev ssl-cert ca-certificates libcurl4

WORKDIR /jsconf-adobe-bot

# Copy build
COPY --from=builder /jsconf-adobe-bot/target/release/jsconf-adobe-bot ./
COPY --from=builder /jsconf-adobe-bot/.env ./

CMD ["/jsconf-adobe-bot/jsconf-adobe-bot"]